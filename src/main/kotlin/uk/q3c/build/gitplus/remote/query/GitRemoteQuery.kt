package uk.q3c.build.gitplus.remote.query

import org.gitlab4j.api.models.Group

/**
 * Used when simply looking for information from a Git Remote provider, rather than manipulating anything.  These should be limited to general queries which
 * do not relate to any specific project
 *
 * Created by David Sowerby on 11 Sep 2018
 */
interface GitRemoteQuery {

    fun groupFromPath(fqPath: String): Group
    fun groups(): List<Group>
}

