package uk.q3c.build.gitplus.local

import io.kotlintest.shouldBe
import io.kotlintest.specs.BehaviorSpec

/**
 * Just verifies the use of [ModelMapper]
 *
 * Created by David Sowerby on 15 Sep 2018
 */
class DefaultGitLocalConfigurationCopyTest : BehaviorSpec() {

    init {
        Given("random changes applied to source") {
            val sourceConfig = DefaultGitLocalConfiguration()
            sourceConfig.projectName = "wiggly"
            sourceConfig.version = 22
            val targetConfig = DefaultGitLocalConfiguration()

            When("copy is made") {
                targetConfig.copyFrom(sourceConfig)

                Then("they are the same") {
                    targetConfig.shouldBe(sourceConfig)
                }
            }


        }
    }
}