package uk.q3c.build.gitplus.remote

import java.net.URI

/**
 * This implementation would need to be replaced if there were more than one remote source (for example BitBucket)
 *
 *
 * Created by David Sowerby on 16 Mar 2016
 */
abstract class DefaultGitRemoteUrlMapper(val gitRemoteServiceProviderResolver: GitRemoteServiceProviderResolver) : GitRemoteUrlMapper {
    override lateinit var remoteConfiguration: GitRemoteConfiguration


    /**
     * This is the same for both GitHub and BitBucket
     */
    override val projectUrl: String
        get () {
            return remoteConfiguration.remoteProjectUri
        }


    override val cloneUrl: String
        get() {
            return "$projectUrl.git"
        }


    override val tagsUrl: String
        get() {
            return "$projectUrl/tree/"
        }

    override val issuesUrl: String
        get () {
            return "$projectUrl/issues/"
        }

    override val wikiUrl: String
        get () {
            return "$projectUrl/wiki"
        }

    override val wikiCloneUrl: String
        get () {
            return "$projectUrl.wiki.git"
        }

    override fun tagUrl(tag: String): String {
        TODO()
    }

    override fun issueUrl(issueNumber: Int): String {
        return "$issuesUrl/$issueNumber"
    }

    override val projectName: String
        get() {
            val uri = URI(projectUrl)
            val p = uri.schemeSpecificPart
            return p.split("/").last()
        }

    override val namespace: String
        get() {
            val uri = URI(projectUrl)
            val p = uri.path
            return p.removeSuffix("/$projectName").removePrefix(("/"))
        }

    override val serviceProvider: ServiceProvider
        get() {
            return gitRemoteServiceProviderResolver.serviceProviderFor(URI(remoteConfiguration.remoteProjectUri))
        }
}


data class NamespaceProject(val namespace: String, val project: String)



