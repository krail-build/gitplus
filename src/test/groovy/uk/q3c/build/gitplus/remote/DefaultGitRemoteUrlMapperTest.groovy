package uk.q3c.build.gitplus.remote

import spock.lang.Specification
import uk.q3c.build.gitplus.remote.github.GitHubUrlMapper

/**
 * Created by David Sowerby on 18 Mar 2016
 */
class DefaultGitRemoteUrlMapperTest extends Specification {

    GitRemoteUrlMapper mapper
    GitRemoteConfiguration remote = new DefaultGitRemoteConfiguration()

    def setup() {
        mapper = new GitHubUrlMapper(new DefaultGitRemoteServiceProviderResolver())
        mapper.remoteConfiguration = remote
        remote.remoteProjectUri("https://github.com/davidsowerby/krail")

    }


    def "Urls with GitHub (default)"() {
        given:
        remote.remoteRepoFullName() >> 'davidsowerby/krail'
        final String cloneUrl = 'https://github.com/davidsowerby/krail.git'
        final String apiUrl = 'https://api.github.com'
        final String repoBaseUrl = 'https://github.com/davidsowerby/krail'
        final String tagUrl = 'https://github.com/davidsowerby/krail/tree/'
        final String issuesUrl = 'https://github.com/davidsowerby/krail/issues/'
        final String wikiUrl = 'https://github.com/davidsowerby/krail/wiki'
        final String wikiCloneUrl = 'https://github.com/davidsowerby/krail.wiki.git'


        expect:
        mapper.apiUrl == apiUrl
        mapper.projectUrl == repoBaseUrl
        mapper.issuesUrl == issuesUrl
        mapper.tagsUrl == tagUrl
        mapper.wikiCloneUrl == wikiCloneUrl
        mapper.wikiUrl == wikiUrl
        mapper.cloneUrl == cloneUrl
    }


}
