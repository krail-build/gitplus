package uk.q3c.build.gitplus.remote.query

import com.google.inject.Inject
import org.gitlab4j.api.GitLabApi
import org.gitlab4j.api.models.Group
import org.gitlab4j.api.models.Project
import org.gitlab4j.api.models.User
import org.slf4j.LoggerFactory
import uk.q3c.build.gitplus.remote.NamespaceProject
import uk.q3c.build.gitplus.remote.RemoteStatus
import uk.q3c.build.gitplus.remote.gitlab.GitLabProvider
import uk.q3c.build.gitplus.util.PropertiesResolver

/**
 * Created by David Sowerby on 11 Sep 2018
 */
class GitLabQuery @Inject constructor(propertiesResolver: PropertiesResolver, gitLabProvider: GitLabProvider) : GitRemoteQuery {
    private val log = LoggerFactory.getLogger(this.javaClass.name)
    val gitLab: GitLabApi

    init {
        propertiesResolver.useGradleFile()
        propertiesResolver.useEnvironmentVariables()
        gitLab = gitLabProvider.get(propertiesResolver)
    }


    /**
     * [fqPath] must contain the namespace if there is one
     *
     * @throws NoSuchElementException if not found
     */
    override fun groupFromPath(fqPath: String): Group {
        if (fqPath.isEmpty()) {
            throw IllegalArgumentException("path should not be empty")
        }
        val g = gitLab.groupApi.getGroup(fqPath)
        if (g == null) {
            throw NoSuchElementException(fqPath)
        } else {
            return g
        }
    }

    /**
     * Returns all groups, whether nested on or not
     *
     * @see rootGroups
     */
    override fun groups(): List<Group> {
        return gitLab.groupApi.groups
    }

    /**
     * returns only those groups at a 'root' level
     */
    fun rootGroups(): List<Group> {
        return groups().filter { g -> g.parentId == null }
    }

    fun projectFromPath(np: NamespaceProject): Project {
        return projectFromPath(np.namespace, np.project)
    }

    fun projectFromPath(namespace: String, path: String): Project {
        val g = gitLab.projectApi.getProject(namespace, path)
        if (g == null) {
            throw NoSuchElementException(path)
        } else {
            return g
        }
    }

    /**
     * Note that [fqPath] must include the namespace if you are looking for a sub-group
     */
    fun groupExists(fqPath: String): Boolean {
        try {
            groupFromPath(fqPath)
        } catch (e: Exception) {
            return false
        }

        return true
    }

    fun currentUser(): User {
        log.debug("retrieving current user")
        val user = gitLab.userApi.currentUser
        if (user == null) {
            throw NoSuchElementException("current user")
        } else {
            log.debug("current user identified")
            return user
        }
    }

    fun apiStatus(): RemoteStatus {
        try {
            if (gitLab.apiVersion.name == "V4") {
                return RemoteStatus.GREEN
            }
            return RemoteStatus.RED
        } catch (e: Exception) {
            return RemoteStatus.RED
        }
    }
}





