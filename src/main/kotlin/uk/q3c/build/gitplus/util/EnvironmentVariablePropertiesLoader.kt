package uk.q3c.build.gitplus.util

import uk.q3c.build.gitplus.remote.ServiceProvider
import java.util.*

/**
 * Created by David Sowerby on 18 Oct 2018
 */
class EnvironmentVariablePropertiesLoader : PropertiesLoader {
    override val properties: Properties
        get() {
            val props = Properties()
            System.getenv().forEach { k, v -> props.setProperty(k, v) }
            return Properties()
        }


    override fun source(sourcePath: String): PropertiesLoader {
        throw UnsupportedOperationException("source is not relevant to enviromnet variables")
    }

    override fun getPropertyValue(property: GitPlusProperty, serviceProvider: ServiceProvider): String {
        val propertyName = propertyKeyLookup(property, serviceProvider)
        val value = System.getenv(propertyName)
        if (value == null) {
            return ""
        } else {
            return value
        }
    }

}