package uk.q3c.build.gitplus.remote.query

import com.google.inject.Guice
import io.kotlintest.matchers.boolean.shouldBeFalse
import io.kotlintest.matchers.boolean.shouldBeTrue
import io.kotlintest.shouldBe
import io.kotlintest.shouldThrow
import io.kotlintest.specs.BehaviorSpec
import org.gitlab4j.api.GitLabApiException
import uk.q3c.build.gitplus.GitPlusModule
import uk.q3c.build.gitplus.remote.RemoteStatus

/**
 * Created by David Sowerby on 11 Sep 2018
 */
class GitLabQueryTest : BehaviorSpec() {

    init {
        val query: GitLabQuery
        val injector = Guice.createInjector(GitPlusModule())
        query = injector.getInstance(GitLabQuery::class.java)



        Given("retrieving a valid group / project") {

            When("1st level group ") {
                val group = query.groupFromPath("kaymanclubs")


                Then("valid group is returned") {
                    group.path.shouldBe("kaymanclubs")
                    group.name.shouldBe("kaymanclubs")
                }
            }


            When("2nd level group ") {
                val group = query.groupFromPath("kaymanclubs/core")

                Then("valid group is returned") {
                    group.path.shouldBe("core")
                    group.name.shouldBe("core")
                    group.fullPath.shouldBe("kaymanclubs/core")
                }
            }

        }

        Given("Invalid inputs") {

            When("input is empty") {
                Then("IllegalArgumentException is thrown") {
                    val exception = shouldThrow<IllegalArgumentException> { query.groupFromPath("") }
                    exception.message.shouldBe("path should not be empty")
                }
            }

            When("input is not found") {
                Then("GitLabApiException is thrown") {
                    shouldThrow<GitLabApiException> { query.groupFromPath("rubbish") }
                }
            }


            When("requesting a group that is a project") {

                Then("GitLabApiException is thrown") {
                    val exception = shouldThrow<GitLabApiException> { query.groupFromPath("kayman-group/club-account/club-account-ui") }
                    exception.message.shouldBe("404 Group Not Found")
                }
            }


        }
        Given("We want to know whether groups exist") {
            When("We check whether groups exist or not") {
                Then("group does exist") {
                    query.groupExists("kaymanclubs").shouldBeTrue()
                    query.groupExists("kaymanclubs/core/club-account").shouldBeTrue()
                }

                Then("a sub-group without namespace will not exist") {
                    query.groupExists("club-account").shouldBeFalse()
                }

                Then("group does not exist") {
                    query.groupExists("rubbish").shouldBeFalse()
                    query.groupExists("kaymanclubs/rubbish").shouldBeFalse()
                }
            }
        }

        Given("identify current user") {
            When("We check for current user") {
                Then("current user should be identified") {
                    query.currentUser().username.shouldBe("dsowerby")
                }
            }
        }

        Given("getting groups returns all groups, rootGroups() only those at root level") {

            When("groups() called") {
                val groups = query.groups()

                Then("it returns all groups") {
                    groups.first { g -> g.path == "kaymanclubs" }
                    groups.first { g -> g.path == "club-account" }

                }
            }



            When("rootGroups called") {
                val groups = query.rootGroups()

                Then("it returns a root group") {
                    groups.first { g -> g.path == "kaymanclubs" }
                }

                Then("but not a nested group") {
                    shouldThrow<NoSuchElementException> { groups.first { g -> g.path == "club-account" } }
                }
            }


        }

        Given("Health Ok") {
            When("calling for status") {
                val status = query.apiStatus()
                Then("Shows green") {
                    status.shouldBe(RemoteStatus.GREEN)
                }
            }

        }
    }
}