package uk.q3c.build.gitplus.remote.gitlab

import com.google.inject.Guice
import io.kotlintest.Description
import io.kotlintest.Spec
import io.kotlintest.extensions.TestListener
import io.kotlintest.matchers.string.shouldStartWith
import io.kotlintest.shouldBe
import io.kotlintest.specs.BehaviorSpec
import org.apache.commons.io.FileUtils
import uk.q3c.build.gitplus.GitPlusModule
import uk.q3c.build.gitplus.gitplus.GitPlus
import java.io.File
import java.util.*


/**
 * Created by David Sowerby on 11 Sep 2018
 */
class DefaultGitLabRemoteTest : BehaviorSpec(), TestListener {

    lateinit var gitPlus1: GitPlus
    lateinit var gitPlus2: GitPlus
    lateinit var tempDir: File
    val groupName = "https://gitlab.com/wigglybits${Random().nextInt()}/mid-group-${Random().nextInt()}"

    override fun beforeSpec(description: Description, spec: Spec) {
        tempDir = File(FileUtils.getTempDirectory(), "${Random().nextInt()}")
        FileUtils.forceMkdir(tempDir)
        val injector = Guice.createInjector(GitPlusModule())
        gitPlus1 = injector.getInstance(GitPlus::class.java)
        gitPlus1.propertiesFromGradle()

        gitPlus2 = injector.getInstance(GitPlus::class.java)
        gitPlus2.propertiesFromGradle()
    }

    override fun afterSpec(description: Description, spec: Spec) {
        FileUtils.forceDelete(tempDir)
    }

    init {


        Given("Creating a project with a new root group") {

            When("Project created using namespace and project name, Groups, project and issue exists") {
                gitPlus1.propertiesFromGradle()
                gitPlus1.local.projectDirParent = tempDir
                gitPlus1.remote.remoteProjectUri = "$groupName/test-project"
                gitPlus1.local.create(true)
                gitPlus1.remote.create(true)
                gitPlus1.execute()

                Then("groups and project are created") {
                    val createdIssue = gitPlus1.remote.createIssue("A random failure", "A problem purely for testing", "bug", "task")

                    val namespace = gitPlus1.remote.namespace

                    val query = (gitPlus1.remote as GitLabRemote).query
                    query.groupFromPath(namespace).path.shouldStartWith("mid-group")
                    query.projectFromPath(namespace, "test-project").path.shouldBe("test-project")

                    val retrievedIssue = gitPlus1.remote.getIssue(createdIssue.number)
                    retrievedIssue.number.shouldBe(1)
                }

            }

            When("we already have the group and create a second project") {
                gitPlus2.propertiesFromGradle()
                gitPlus2.local.projectDirParent = tempDir
                gitPlus2.remote.remoteProjectUri = "$groupName/test-project2"
                gitPlus2.local.create(true)
                gitPlus2.remote.create(true)
                gitPlus2.execute()

                Then("the second project is created successfully") {
                    val query = (gitPlus2.remote as GitLabRemote).query
                    val namespace = gitPlus2.remote.namespace
                    query.groupFromPath(namespace).path.shouldStartWith("mid-group")
                    query.projectFromPath(namespace, "test-project2").path.shouldBe("test-project2")
                }

            }

            When("repo deleted") {
                val query = (gitPlus1.remote as GitLabRemote).query
                val primaryGroupName = gitPlus1.remote.namespace.split("/")[0]
                val groupToDelete = query.groupFromPath(primaryGroupName)
                val confirmation = "I really, really want to delete the $primaryGroupName group from GitLab"
                gitPlus1.remote.deleteGroup(group = groupToDelete, confirmationMessage = confirmation)

                Then("remote project has gone, though we may have to wait") {
                    var gone = false
                    var count = 0
                    while (!gone && count < 10) {
                        gone = !query.groupExists(primaryGroupName)
                        Thread.sleep(1000)
                        count++
                    }
                }
            }


        }
    }

}

