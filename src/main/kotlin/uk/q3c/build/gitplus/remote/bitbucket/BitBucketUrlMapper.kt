package uk.q3c.build.gitplus.remote.bitbucket

import com.google.inject.Inject
import uk.q3c.build.gitplus.remote.DefaultGitRemoteUrlMapper
import uk.q3c.build.gitplus.remote.GitRemoteServiceProviderResolver

/**
 * Created by David Sowerby on 26 Oct 2016
 */
class BitBucketUrlMapper @Inject constructor(gitRemoteServiceProviderResolver: GitRemoteServiceProviderResolver) : DefaultGitRemoteUrlMapper(gitRemoteServiceProviderResolver) {
    override val apiUrl: String
        get() = TODO("not implemented") //To change initializer of created properties use File | Settings | File Templates.

}