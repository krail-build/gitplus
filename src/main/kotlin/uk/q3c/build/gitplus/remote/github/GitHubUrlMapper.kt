package uk.q3c.build.gitplus.remote.github

import com.google.inject.Inject
import uk.q3c.build.gitplus.remote.DefaultGitRemoteUrlMapper
import uk.q3c.build.gitplus.remote.GitRemoteServiceProviderResolver

/**
 * Created by David Sowerby on 26 Oct 2016
 */
class GitHubUrlMapper @Inject constructor(gitRemoteServiceProviderResolver: GitRemoteServiceProviderResolver) : DefaultGitRemoteUrlMapper(gitRemoteServiceProviderResolver) {

    override val apiUrl: String
        get () {
            return "https://api.github.com"
        }

    companion object {

        val STATUS_API_URL = "https://status.github.com/api/status.json"

    }
}