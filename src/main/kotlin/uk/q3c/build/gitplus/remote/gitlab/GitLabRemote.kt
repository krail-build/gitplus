package uk.q3c.build.gitplus.remote.gitlab

import com.google.inject.Inject
import org.eclipse.jgit.transport.CredentialsProvider
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider
import org.gitlab4j.api.GitLabApi
import org.gitlab4j.api.models.Branch
import org.gitlab4j.api.models.Group
import org.gitlab4j.api.models.Project
import org.slf4j.LoggerFactory
import uk.q3c.build.gitplus.GitSHA
import uk.q3c.build.gitplus.gitplus.GitPlus
import uk.q3c.build.gitplus.gitplus.GitPlusConfigurationException
import uk.q3c.build.gitplus.gitplus.GitPlusException
import uk.q3c.build.gitplus.local.GitBranch
import uk.q3c.build.gitplus.notSpecified
import uk.q3c.build.gitplus.remote.AbstractGitRemote
import uk.q3c.build.gitplus.remote.GPIssue
import uk.q3c.build.gitplus.remote.GitRemote
import uk.q3c.build.gitplus.remote.GitRemoteConfiguration
import uk.q3c.build.gitplus.remote.GitRemoteException
import uk.q3c.build.gitplus.remote.GitRemoteUrlMapper
import uk.q3c.build.gitplus.remote.NamespaceProject
import uk.q3c.build.gitplus.remote.RemoteStatus
import uk.q3c.build.gitplus.remote.ServiceProvider
import uk.q3c.build.gitplus.remote.query.GitLabQuery

/**
 * Created by David Sowerby on 10 Sep 2018
 */
interface GitLabRemote : GitRemote {
    val query: GitLabQuery
}

class DefaultGitLabRemote @Inject constructor(
        override val configuration: GitRemoteConfiguration,
        val gitLabProvider: GitLabProvider,
        override val query: GitLabQuery,
        val projectCreator: GitLabProjectCreator,
        val groupCreator: GitLabGroupCreator,
        override val urlMapper: GitLabUrlMapper) :
        AbstractGitRemote(),
        GitLabRemote,
        GitRemoteConfiguration by configuration,
        GitRemoteUrlMapper by urlMapper {


    private val log = LoggerFactory.getLogger(this.javaClass.name)
    override lateinit var parent: GitPlus
    override var projectCreated: Boolean = false

    init {
        urlMapper.remoteConfiguration = this
    }


    override fun hasBranch(branch: GitBranch): Boolean {
        val project = gitLab().projectApi.getProject(namespace, projectName)
        try {
            val b = gitLab().repositoryApi.getBranch(project.id, branch.name)
            return b != null
        } catch (e: Exception) {
            return false
        }
    }

    override fun getIssue(issueNumber: Int): GPIssue {
        log.debug("Retrieving issue number $issueNumber")
        val project = gitLab().projectApi.getProject(namespace, projectName)
        val issue = gitLab().issuesApi.getIssue(project.id, issueNumber)
        return GPIssue(issue)
    }

    private fun gitLab(): GitLabApi {
        return gitLabProvider.get(parent)
    }

    override fun getIssue(remoteNamespace: String, remoteProjectName: String, issueNumber: Int): GPIssue {
        log.debug("retrieving issue $issueNumber from $remoteNamespace/$remoteProjectName")
        val project = gitLab().projectApi.getProject(remoteNamespace, remoteProjectName)
        val issue = gitLab().issuesApi.getIssue(project.id, issueNumber)
        return GPIssue(issue)
    }

    override val credentialsProvider: CredentialsProvider
        get() {
            try {
                val token = parent.apiTokenIssueCreate(ServiceProvider.GITLAB)
                return UsernamePasswordCredentialsProvider("GitPlus", token)
            } catch (e: Exception) {
                throw GitRemoteException("An api token is required in order to enable credentials", e)
            }

        }

    /**
     * Simplistic check - cannot find a better way, as healthcheck api does not work on public GitLab
     */
    override fun apiStatus(): RemoteStatus {
        return query.apiStatus()
    }

    override fun createIssue(issueTitle: String, body: String, vararg labels: String): GPIssue {
        log.debug("creating issue in current project of $namespace/$projectName")
        val project = gitLab().projectApi.getProject(namespace, projectName)
        val labelsCSV = if (labels.isEmpty()) {
            null
        } else {
            labels.joinToString(separator = ",")
        }

        val issue = gitLab().issuesApi.createIssue(project.id, issueTitle, body, null, null, null, labelsCSV, null, null, null, null)
        log.debug("issue ${issue.iid} created")
        return GPIssue(issue)
    }

    override fun createProject() {
        if (!projectCreated) {
            val project = Project()
            project.defaultBranch = defaultBranch
            project.description = projectDescription
            project.issuesEnabled = true
            project.public = publicProject
            project.wikiEnabled = true
            project.path = projectName
            project.name = project.path


            if (namespace == notSpecified || namespace == "" || namespace == query.currentUser().username) {
                projectCreator.create(project, gitLab())
            } else {
                val groupId = createGroupsForNamespace()
                projectCreator.create(groupId, project, gitLab())
            }
            projectCreated = true
        } else {
            log.info("repeated call to create project ignored")
        }
    }

    /**
     * Creating a repo may not be instantaneous
     */
    override fun waitForRemoteRepo(newProject: Project, retries: Int, waitInterval: Long) {

        var branches: List<Branch> = mutableListOf()
        var r = retries
        while (branches.isEmpty() && r >= 0) {
            try {
                log.debug("Waiting for remote repository to be available, $r tries left")
                branches = gitLab().repositoryApi.getBranches(newProject.id)
                r--
            } catch (e: Exception) {
                r--
                Thread.sleep(waitInterval)
            }
        }
        if (branches.isEmpty()) {
            throw GitRemoteException("Timed out trying to create remote project")
        }
    }

    override fun setDefaultBranch() {
        val project = query.projectFromPath(NamespaceProject(namespace, projectName))
        var branch: Branch
        try {
            branch = gitLab().repositoryApi.getBranch(project.id, defaultBranch)
        } catch (e: Exception) {
            branch = gitLab().repositoryApi.createBranch(project.id, defaultBranch, "master")
        }
        project.defaultBranch = branch.name
        gitLab().projectApi.updateProject(project)
    }

    /**
     *
     *
     * Before we get here we know that [namespace] is not empty, is != 'not specified' or matching the current username
     *
     * We iterate through the path making sure there is either an existing group, or creating one if not
     *
     * @return the group Id for last group in the chain (therefore the one which contains the project).
     *
     */
    private fun createGroupsForNamespace(): Int {
        val segments = namespace.split("/")
        var groupId: Int

        // if the root group exists, just hold the group id, else create a new root
        if (query.groupExists(segments[0])) {
            groupId = query.groupFromPath(segments[0]).id
        } else {
            val group = Group()
            group.path = segments[0]
            group.name = segments[0]
            groupId = groupCreator.create(group, gitLab()).id
        }

        // for each sub-group, create as needed to complete the path
        var n = 1
        while (n < segments.size) {
            val fullGroupPath = segments.subList(0, n + 1).joinToString(separator = "/")
            if (query.groupExists(fullGroupPath)) {
                groupId = query.groupFromPath(fullGroupPath).id
                n++
            } else {
                val newGroup = Group()
                newGroup.path = segments[n]
                newGroup.name = segments[n]
                groupId = groupCreator.create(groupId, newGroup, gitLab()).id
                n++
            }
        }

        return groupId
    }

    override fun deleteProject() {
        try {
            if (configuration.deleteRepoApproved()) {
                gitLab().projectApi.deleteProject(currentProject())
            } else {
                throw GitRemoteException("Repo deletion not confirmed")
            }
        } catch (e: Exception) {
            throw GitRemoteException("Unable to delete remote repo", e)
        }
    }

    override fun listRepositoryNames(): Set<String> {
        return gitLab().projectApi.projects.map { p -> p.name }.toSet()
    }

    override fun mergeLabels() {
        throw UnsupportedOperationException("May not need this, labels are inherited from group")
    }

    override fun mergeLabels(labelsToMerge: Map<String, String>): Map<String, String> {
        throw UnsupportedOperationException("May not need this, labels are inherited from group")
    }

    override val labelsAsMap: Map<String, String>
        get() {
            val labels = gitLab().labelsApi.getLabels(currentProject().id)
            val map: MutableMap<String, String> = mutableMapOf()
            labels.forEach { l -> map.put(l.name, l.color) }
            return map
        }

    private fun currentProject(): Project {
        return query.projectFromPath(namespace, projectName)
    }

    override fun developHeadCommit(): GitSHA {
        return headCommit(GitBranch("develop"))
    }

    override fun headCommit(branch: GitBranch): GitSHA {
        val b = gitLab().repositoryApi.getBranch(currentProject().id, branch.name)
        return GitSHA(b.commit.id)
    }


    override fun prepare(parent: GitPlus) {
        this.parent = parent
    }

    override fun verifyFromLocal() {
        if (parent.local.active) {
            setupFromOrigin(parent.local.getOrigin())
        } else {
            throw GitPlusConfigurationException("GitRemote cannot be verified from a GitLocal which is not active")
        }
    }

    override fun createGroup(): Group {
        log.debug("creating a group for $namespace")
        if (namespace.isEmpty()) {
            throw GitPlusConfigurationException("Cannot create a group with an empty name")
        }
        if (namespace == notSpecified) {
            throw GitPlusConfigurationException("Cannot create a group with an unspecified name")
        }
        if (namespace == query.currentUser().username) {
            throw GitPlusConfigurationException("Cannot create a group '$namespace', it is the same as the user name")
        }
        val groupId = createGroupsForNamespace()
        return gitLab().groupApi.getGroup(groupId)
    }

    override fun deleteGroup(group: Group, confirmationMessage: String) {
        if (configuration.deleteGroupApproved(group, confirmationMessage)) {
            log.info("deleting group: ${group.name}")
            gitLab().groupApi.deleteGroup(group)
        } else {
            throw GitRemoteException("Confirmation of delete operation was incorrect - delete not performed")
        }
    }
}

interface GitLabProjectCreator {
    var gitPlus: GitPlus
    fun create(groupId: Int, project: Project, gitLab: GitLabApi): Project
    fun create(project: Project, gitLab: GitLabApi): Project
}


class DefaultGitLabProjectCreator @Inject constructor() : GitLabProjectCreator {
    private val log = LoggerFactory.getLogger(this.javaClass.name)
    override lateinit var gitPlus: GitPlus
    override fun create(groupId: Int, project: Project, gitLab: GitLabApi): Project {
        log.debug("creating project ${project.path}")
        val newProject = gitLab.projectApi.createProject(groupId, project.path)
        log.debug("project ${newProject.pathWithNamespace} created")
        return newProject
    }

    override fun create(project: Project, gitLab: GitLabApi): Project {
        return gitLab.projectApi.createProject(project)
    }

    private fun updateProjectDetail(from: Project, to: Project) {
        if (from.defaultBranch != null) {
            to.defaultBranch = from.defaultBranch
        }
        if (from.wikiEnabled != null) {
            to.wikiEnabled = from.wikiEnabled
        }
    }
}

interface GitLabGroupCreator {

    fun create(group: Group, gitLab: GitLabApi): Group
    fun create(parentGroup: Int, group: Group, gitLab: GitLabApi): Group
}

class DefaultGitLabGroupCreator @Inject constructor() : GitLabGroupCreator {
    private val log = LoggerFactory.getLogger(this.javaClass.name)
    override fun create(group: Group, gitLab: GitLabApi): Group {
        log.debug("creating group ${group.path}")
        try {
            val g: Group = gitLab.groupApi.addGroup(group)
            log.debug("group ${g.fullPath} created")
            return g
        } catch (e: Exception) {
            throw GitPlusException("Creation of Group ${group.path} failed", e)
        }
    }

    override fun create(parentGroup: Int, group: Group, gitLab: GitLabApi): Group {
        return create(group.withParentId(parentGroup), gitLab)
    }
}