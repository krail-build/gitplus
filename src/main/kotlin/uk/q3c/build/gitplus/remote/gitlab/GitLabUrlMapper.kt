package uk.q3c.build.gitplus.remote.gitlab

import com.google.inject.Inject
import uk.q3c.build.gitplus.remote.DefaultGitRemoteUrlMapper
import uk.q3c.build.gitplus.remote.GitRemoteServiceProviderResolver

/**
 * Created by David Sowerby on 26 Oct 2016
 */
class GitLabUrlMapper @Inject constructor(gitRemoteServiceProviderResolver: GitRemoteServiceProviderResolver) : DefaultGitRemoteUrlMapper(gitRemoteServiceProviderResolver) {


    override val apiUrl: String
        get () {
            return "https://gitlab.com/api/v4"
        }

    override val wikiUrl: String
        get() {
            return "$projectUrl/wikis"
        }

    override val tagsUrl: String
        get() {
            return "$projectUrl/tags/"
        }
}