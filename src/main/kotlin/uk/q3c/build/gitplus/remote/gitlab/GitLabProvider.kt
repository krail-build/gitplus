package uk.q3c.build.gitplus.remote.gitlab

import org.gitlab4j.api.GitLabApi
import uk.q3c.build.gitplus.remote.ServiceProvider
import uk.q3c.build.gitplus.util.PropertiesResolver


/**
 * Created by David Sowerby on 10 Sep 2018
 */
interface GitLabProvider {
    fun get(propertiesResolver: PropertiesResolver): GitLabApi
}

class DefaultGitLabProvider : GitLabProvider {

    override fun get(propertiesResolver: PropertiesResolver): GitLabApi {
        return create(propertiesResolver.apiTokenIssueCreate(ServiceProvider.GITLAB))
    }


    fun create(oauthKey: String): GitLabApi {
        return GitLabApi("https://gitlab.com", oauthKey)
    }
}