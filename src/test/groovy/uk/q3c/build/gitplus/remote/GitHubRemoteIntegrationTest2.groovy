package uk.q3c.build.gitplus.remote

import com.google.inject.Inject
import spock.guice.UseModules
import spock.lang.Specification
import uk.q3c.build.gitplus.GitPlusModule
import uk.q3c.build.gitplus.gitplus.GitPlus
import uk.q3c.build.gitplus.remote.RemoteStatus
/**
 *
 * Created by David Sowerby on 20 Mar 2016
 */
@UseModules([GitPlusModule])
class GitHubRemoteIntegrationTest2 extends Specification {


    @Inject
    GitPlus gitPlus

    def setup() {
        gitPlus.remote.remoteProjectUri(new URI("https://github.com/davidsowerby/krail"))
        gitPlus.propertiesFromGradle()

    }

    def "api status"() {
        given:
        gitPlus.execute()

        expect:
        println gitPlus.remote.apiStatus()
        gitPlus.remote.apiStatus() == RemoteStatus.GREEN
    }

    def "list repos for this user"() {
        when:
        gitPlus.remote.remoteProjectUri(new URI("https://github.com/davidsowerby/krail"))
        gitPlus.propertiesFromGradle()
        gitPlus.execute()
        Set<String> repos = gitPlus.remote.listRepositoryNames()

        then:
        repos.contains('hal-kotlin')
        !repos.contains('perl')
        repos.size() >= 17

    }


}