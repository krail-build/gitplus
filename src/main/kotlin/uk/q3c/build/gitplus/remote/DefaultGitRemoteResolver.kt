package uk.q3c.build.gitplus.remote

import com.google.inject.Inject
import com.google.inject.Provider
import uk.q3c.build.gitplus.gitplus.GitPlusConfigurationException
import java.net.URI

/**
 * Created by David Sowerby on 25 Oct 2016
 */
class DefaultGitRemoteResolver @Inject constructor(override val remotes: MutableMap<ServiceProvider, Provider<GitRemote>>, val serviceProviderResolver: GitRemoteServiceProviderResolver) : GitRemoteResolver {

    override fun defaultProvider(): ServiceProvider {
        return ServiceProvider.GITHUB
    }

    override fun get(configuration: GitRemoteConfiguration): GitRemote {
        val uri: URI
        try {
            uri = URI(configuration.remoteProjectUri)
        } catch (e: Exception) {
            throw GitPlusConfigurationException("${configuration.remoteProjectUri} is not a valid URI for GitPlus.remote.remoteProjectUri")
        }

        val serviceProvider: ServiceProvider = serviceProviderResolver.serviceProviderFor(uri)
        val remoteServiceProvider = remotes.get(serviceProvider)
                ?: throw GitRemoteException("No GitRemote implementation has been defined for $serviceProvider.  Define one in your Guice module")
        // copy any changes which have been made to the configuration
        val remote = remoteServiceProvider.get()
        remote.configuration.copyFrom(configuration)
        return remote
    }

    override fun getDefault(): GitRemote {
        return remotes.get(ServiceProvider.GITHUB)!!.get()
    }
}

class DefaultGitRemoteServiceProviderResolver : GitRemoteServiceProviderResolver {
    override fun serviceProviderFor(uri: URI): ServiceProvider {
        return when {
            uri.host.contains("gitlab") -> ServiceProvider.GITLAB
            uri.host.contains("github") -> ServiceProvider.GITHUB
            uri.host.contains("bitbucket") -> ServiceProvider.BITBUCKET

            else -> {
                ServiceProvider.NOT_SPECIFIED
            }
        }
    }
}


