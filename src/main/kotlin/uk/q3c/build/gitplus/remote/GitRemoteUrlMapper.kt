package uk.q3c.build.gitplus.remote

/**
 * Created by David Sowerby on 16 Mar 2016
 */
interface GitRemoteUrlMapper {

    var remoteConfiguration: GitRemoteConfiguration


    val projectUrl: String

    /**
     * The url to (https) clone this repo, For example: 'https://github.com/davidsowerby/krail.git'
     */
    val cloneUrl: String

    /**
     * The url for the tags (releases) in this remote project, for example: 'https://github.com/davidsowerby/krail/tree/
     */
    val tagsUrl: String

    /**
     * The url to a specific tag in this remote project, for example: 'https://github.com/davidsowerby/krail/tree/0.12.3.0'
     */
    fun tagUrl(tag: String): String

    /**
     * The url to the wiki for this project.  For example: 'https://github.com/davidsowerby/krail/wiki'
     */
    val wikiUrl: String

    /**
     * The api url for the remote service provider, for example 'https://api.github.com/'
     */
    val apiUrl: String

    /**
     * The url to clone the wiki.  For example: 'https://github.com/davidsowerby/krail.wiki.git'
     */
    val wikiCloneUrl: String

    /**
     * The url to the issues for this project, For example, this might be: 'https://github.com/davidsowerby/krail/issues/'  (including the trailing slash)
     */
    val issuesUrl: String

    /**
     * The url to a specific issue, given the [issueNumber]
     */
    fun issueUrl(issueNumber: Int): String

    /**
     * Derives the project name from the [projectUrl]
     */
    val projectName: String

    /**
     * Derives the project namespace from the [projectUrl]
     */

    val namespace: String

    /**
     * GitHub, GitLab etc.  Derived from the [projectUrl]
     */
    val serviceProvider: ServiceProvider
}
