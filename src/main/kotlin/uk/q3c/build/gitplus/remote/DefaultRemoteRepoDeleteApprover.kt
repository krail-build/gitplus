package uk.q3c.build.gitplus.remote

import org.gitlab4j.api.models.Group

/**
 * Created by David Sowerby on 05 Apr 2016
 */
class DefaultRemoteRepoDeleteApprover : RemoteRepoDeleteApprover {
    override fun isApproved(configuration: GitRemoteConfiguration): Boolean {
        val confirmationMessage = "I really, really want to delete the project at: " + configuration.remoteProjectUri
        return confirmationMessage == configuration.confirmProjectDelete
    }
}


class GitLabRemoteProjectDeleteApprover : RemoteRepoDeleteApprover {
    override fun isApproved(configuration: GitRemoteConfiguration): Boolean {
        val confirmationMessage = "I really, really want to delete the project at: " + configuration.remoteProjectUri
        return confirmationMessage == configuration.confirmProjectDelete
    }
}

class GitLabRemoteGroupDeleteApprover : RemoteGroupDeleteApprover {
    override fun isApproved(group: Group, confirmation: String): Boolean {
        val requiredConfirmation = "I really, really want to delete the ${group.name} group from GitLab"
        return confirmation == requiredConfirmation
    }
}
