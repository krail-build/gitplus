package uk.q3c.build.gitplus.remote

import org.gitlab4j.api.models.Group

/**
 * Created by David Sowerby on 05 Apr 2016
 */
interface RemoteRepoDeleteApprover {
    fun isApproved(configuration: GitRemoteConfiguration): Boolean
}

interface RemoteGroupDeleteApprover {
    fun isApproved(group: Group, confirmation: String): Boolean
}
