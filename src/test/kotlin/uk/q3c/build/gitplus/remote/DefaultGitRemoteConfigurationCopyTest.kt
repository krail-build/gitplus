package uk.q3c.build.gitplus.remote

import io.kotlintest.shouldBe
import io.kotlintest.specs.BehaviorSpec

/**
 * Just verifies the use of [ModelMapper]
 *
 * Created by David Sowerby on 15 Sep 2018
 */
class DefaultGitRemoteConfigurationCopyTest : BehaviorSpec({

    Given("Copy from one config to another") {
        Given("random changes applied to source") {
            val sourceConfig = DefaultGitRemoteConfiguration()
            sourceConfig.remoteProjectUri = "https://gitlab.com/wiggly/project"
            val targetConfig = DefaultGitRemoteConfiguration()

            When("copy is made") {
                targetConfig.copyFrom(sourceConfig)


                Then("they are the same") {
                    targetConfig.shouldBe(sourceConfig)
                }
            }

        }
    }
})