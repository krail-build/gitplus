package uk.q3c.build.gitplus.remote

import com.google.inject.Provider
import spock.lang.Specification
import uk.q3c.build.gitplus.remote.bitbucket.BitBucketRemote
import uk.q3c.build.gitplus.remote.github.DefaultGitHubProvider
import uk.q3c.build.gitplus.remote.github.DefaultGitHubRemote
import uk.q3c.build.gitplus.remote.github.GitHubRemote
import uk.q3c.build.gitplus.remote.github.GitHubUrlMapper
import uk.q3c.build.gitplus.remote.gitlab.*
import uk.q3c.build.gitplus.remote.query.GitLabQuery
import uk.q3c.build.gitplus.util.PropertiesResolver

import static uk.q3c.build.gitplus.remote.ServiceProvider.*

/**
 * Created by David Sowerby on 26 Oct 2016
 */
class DefaultGitRemoteResolverTest extends Specification {

    PropertiesResolver apiPropertiesResolver = Mock(PropertiesResolver)
    static GitRemoteConfiguration gitRemoteConfiguration1 = new DefaultGitRemoteConfiguration()
    GitRemoteConfiguration gitRemoteConfiguration2 = new DefaultGitRemoteConfiguration()


    Map<ServiceProvider, Provider<GitRemote>> serviceProviders

    GitHubRemote gitHubRemote = new DefaultGitHubRemote(gitRemoteConfiguration2, new DefaultGitHubProvider(), new DefaultRemoteRequest(), new GitHubUrlMapper(new DefaultGitRemoteServiceProviderResolver()))
    Provider<GitHubRemote> gitHubProvider = Mock(Provider)

    GitLabRemote gitLabRemote = new DefaultGitLabRemote(gitRemoteConfiguration2, new DefaultGitLabProvider(), new GitLabQuery(Mock(PropertiesResolver), Mock(GitLabProvider)), Mock(GitLabProjectCreator), Mock(GitLabGroupCreator), new GitLabUrlMapper(new DefaultGitRemoteServiceProviderResolver()))
    Provider<BitBucketRemote> gitLabProvider = Mock(Provider)


    def setup() {
        gitHubProvider.get() >> gitHubRemote
        gitLabProvider.get() >> gitLabRemote
        serviceProviders = new HashMap<>()
        serviceProviders.put(GITHUB, gitHubProvider)
        serviceProviders.put(GITLAB, gitLabProvider)

    }

    def "change remote service provider"() {

        given:
        gitRemoteConfiguration1.remoteProjectUri("https://gitlab.com/davidsowerby/wiggly")
        gitRemoteConfiguration2.remoteProjectUri("https://github.com/dsowerby/wiggly2")
        GitRemoteResolver provider = new DefaultGitRemoteResolver(serviceProviders, new DefaultGitRemoteServiceProviderResolver())

        when:
        GitRemote remote1 = provider.get(gitRemoteConfiguration1)

        then:
        GitLabRemote.class.isAssignableFrom(remote1.class)
        remote1.namespace == "davidsowerby"
        remote1.projectName == "wiggly"
        gitRemoteConfiguration1 == gitRemoteConfiguration2


    }
}
