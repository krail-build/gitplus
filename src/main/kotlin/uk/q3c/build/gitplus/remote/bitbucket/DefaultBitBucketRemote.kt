package uk.q3c.build.gitplus.remote.bitbucket

import com.google.inject.Inject
import org.eclipse.jgit.transport.CredentialsProvider
import org.gitlab4j.api.models.Group
import org.gitlab4j.api.models.Project
import uk.q3c.build.gitplus.GitSHA
import uk.q3c.build.gitplus.gitplus.GitPlus
import uk.q3c.build.gitplus.local.GitBranch
import uk.q3c.build.gitplus.remote.GPIssue
import uk.q3c.build.gitplus.remote.GitRemoteConfiguration
import uk.q3c.build.gitplus.remote.GitRemoteUrlMapper
import uk.q3c.build.gitplus.remote.RemoteRequest
import uk.q3c.build.gitplus.remote.RemoteStatus

/**
 * Created by David Sowerby on 25 Oct 2016
 */
class DefaultBitBucketRemote @Inject constructor(override val configuration: GitRemoteConfiguration, val bitBucketProvider: BitBucketProvider, val remoteRequest: RemoteRequest, override val urlMapper: BitBucketUrlMapper) :
        BitBucketRemote,
        GitRemoteConfiguration by configuration, GitRemoteUrlMapper by urlMapper {
    override var projectCreated: Boolean = false


    override fun waitForRemoteRepo(newProject: Project, retries: Int, waitInterval: Long) {
        TODO()
    }

    override fun setDefaultBranch() {
        TODO()
    }

    override fun createGroup(): Group {
        TODO()
    }

    override fun deleteGroup(group: Group, confirmationMessage: String) {
        TODO()
    }

    override lateinit var parent: GitPlus

    override fun verifyFromLocal() {
        TODO()
    }


    override fun prepare(parent: GitPlus) {
        TODO()
//        validate()
    }

    override fun hasBranch(branch: GitBranch): Boolean {
        TODO()
    }

    override fun headCommit(branch: GitBranch): GitSHA {
        TODO()
    }

    override fun isIssueFixWord(word: String): Boolean {
        TODO()
    }

    override fun getIssue(issueNumber: Int): GPIssue {
        TODO()
    }

    override fun getIssue(remoteNamespace: String, remoteProjectName: String, issueNumber: Int): GPIssue {
        TODO()
    }

    override val credentialsProvider: CredentialsProvider
        get() = throw NotImplementedError()

    override fun apiStatus(): RemoteStatus {
        TODO()
    }

    override fun createIssue(issueTitle: String, body: String, vararg labels: String): GPIssue {
        TODO()
    }

    override fun createProject() {
        TODO()
    }

    override fun deleteProject() {
        TODO()
    }

    override fun listRepositoryNames(): Set<String> {
        TODO()
    }

    override fun mergeLabels() {
        TODO()
    }

    override fun mergeLabels(labelsToMerge: Map<String, String>): Map<String, String> {
        TODO()
    }

    override val labelsAsMap: Map<String, String>
        get() = throw NotImplementedError()

    override fun developHeadCommit(): GitSHA {
        TODO()
    }
}