package uk.q3c.build.gitplus.remote

import com.fasterxml.jackson.annotation.JsonIgnore
import com.google.common.collect.ImmutableMap
import org.gitlab4j.api.models.Group
import org.modelmapper.ModelMapper
import uk.q3c.build.gitplus.gitplus.GitPlusConfigurationException
import uk.q3c.build.gitplus.local.GitLocal
import uk.q3c.build.gitplus.notSpecified
import java.net.URI


/**
 * Created by David Sowerby on 16 Oct 2016
 */
data class DefaultGitRemoteConfiguration @JvmOverloads constructor(override var remoteProjectUri: String = notSpecified)

    : GitRemoteConfiguration {


    /**
     * Not supported for gitHub https://gitlab.com/krail-build/gitplus/issues/177
     */
    override var defaultBranch: String = "develop"
    override var version = 1
    override var active = true
    override var confirmProjectDelete: String = ""
    @Transient
    @JsonIgnore
    override var repoDeleteApprover: RemoteRepoDeleteApprover = DefaultRemoteRepoDeleteApprover()

    @Transient
    @JsonIgnore
    override var groupDeleteApprover: RemoteGroupDeleteApprover = GitLabRemoteGroupDeleteApprover()

    override var projectDescription = ""
    override var projectHomePage = ""
    override var publicProject: Boolean = false

    override var create: Boolean = false
    override var issueLabels: MutableMap<String, String> = defaultIssueLabels
    override var mergeIssueLabels = false


    override fun deleteRepoApproved(): Boolean {
        return repoDeleteApprover.isApproved(this)
    }

    override fun deleteGroupApproved(group: Group, confirmationMessage: String): Boolean {
        return groupDeleteApprover.isApproved(group = group, confirmation = confirmationMessage)
    }


    /**
     * The issueLabels field is set to an immutable copy of [issueLabels]
     *
     * @param issueLabels the issues to use
     *
     * @return this for fluency
     */
    override fun issueLabels(issueLabels: MutableMap<String, String>): GitRemoteConfiguration {
        this.issueLabels = ImmutableMap.copyOf(issueLabels).toMutableMap()
        return this
    }

    override fun mergeIssueLabels(mergeIssueLabels: Boolean): GitRemoteConfiguration {
        this.mergeIssueLabels = mergeIssueLabels
        return this
    }

    override fun active(value: Boolean): GitRemoteConfiguration {
        this.active = value
        return this
    }


    override fun create(value: Boolean): GitRemoteConfiguration {
        this.create = value
        return this
    }


    override fun projectDescription(projectDescription: String): GitRemoteConfiguration {
        this.projectDescription = projectDescription
        return this
    }

    override fun projectHomePage(projectHomePage: String): GitRemoteConfiguration {
        this.projectHomePage = projectHomePage
        return this
    }

    override fun publicProject(publicProject: Boolean): GitRemoteConfiguration {
        this.publicProject = publicProject
        return this
    }

    override fun confirmDelete(confirmRemoteDelete: String): GitRemoteConfiguration {
        this.confirmProjectDelete = confirmRemoteDelete
        return this
    }


    override fun repoDeleteApprover(repoDeleteApprover: RemoteRepoDeleteApprover): GitRemoteConfiguration {
        this.repoDeleteApprover = repoDeleteApprover
        return this
    }

    override fun setupFromOrigin(origin: String) {
        remoteProjectUri = origin
    }

    override fun remoteProjectUri(uri: URI): GitRemoteConfiguration {
        remoteProjectUri = uri.toString()
        return this
    }

    override fun remoteProjectUri(uri: String): GitRemoteConfiguration {
        remoteProjectUri = URI(uri).toString() // conversion for syntax check
        return this
    }

    override fun validate(local: GitLocal) {

        if (remoteProjectUri.isEmpty() || remoteProjectUri == notSpecified) {
            if (local.active) {
                try {
                    val origin = local.getOrigin()
                    remoteProjectUri(URI(origin))
                } catch (e: Exception) {
                    throw GitPlusConfigurationException("unable to set remoteProjectUri from local origin")
                }
            } else {
                throw GitPlusConfigurationException("remoteProjectUri cannot be empty")
            }
        }
    }


    override fun copyFrom(other: GitRemoteConfiguration) {
        val modelMapper = ModelMapper()
        modelMapper.map(other, this)
    }


    companion object {

        val defaultIssueLabels: MutableMap<String, String> = ImmutableMap.Builder<String, String>().put("bug", "fc2929").put("duplicate", "cccccc").put("enhancement", "84b6eb").put("question", "cc317c").put("wontfix", "d7e102").put("task", "0b02e1").put("quality", "02d7e1").put("documentation", "eb6420").put("build", "fbca04").put("performance", "d4c5f9").put("critical", "e11d21").build().toMutableMap()
    }


}

