package uk.q3c.build.gitplus.remote

import com.fasterxml.jackson.databind.ObjectMapper
import com.google.common.collect.ImmutableMap
import spock.lang.Specification
import uk.q3c.build.gitplus.gitplus.GitPlusConfigurationException
import uk.q3c.build.gitplus.local.GitLocal
/**
 * Created by David Sowerby on 31 Oct 2016
 */
class DefaultGitRemoteConfigurationTest extends Specification {

    RemoteRepoDeleteApprover approver = Mock(RemoteRepoDeleteApprover)
    GitRemoteConfiguration configuration

    void setup() {
        configuration = new DefaultGitRemoteConfiguration()
    }

    def "DeleteRepoApproved"() {
        given:
        approver.isApproved(configuration) >> approved

        when:
        configuration.repoDeleteApprover(approver)

        then:
        configuration.deleteRepoApproved() == result

        where:

        approved | result
        true     | true
        false    | false

    }

    def "JSON export import"() {
        given: "change to a couple of non-default values"
        ObjectMapper objectMapper = new ObjectMapper()
        StringWriter sw = new StringWriter()
        configuration.remoteProjectUri(new URI("https://github.com/davidsowerby/wiggly")).projectHomePage('funky.pigeon')
        configuration.version = 99

        when:
        objectMapper.writeValue(sw, configuration)
        DefaultGitRemoteConfiguration configuration2 = objectMapper.readValue(sw.toString(), DefaultGitRemoteConfiguration.class)

        then:
        configuration == configuration2
        configuration2.version == 99
    }

    def "IssueLabels"() {
        given:
        Map<String, String> map = ImmutableMap.of()

        when:
        configuration.issueLabels(map)

        then:
        configuration.issueLabels == map
    }

    def "MergeIssueLabels"() {

        when:
        configuration.mergeIssueLabels(true)

        then:
        configuration.mergeIssueLabels

        when:
        configuration.mergeIssueLabels(false)

        then:
        !configuration.mergeIssueLabels
    }

    def "Create"() {
        when:
        configuration.create(true)

        then:
        configuration.create

        when:
        configuration.create(false)

        then:
        !configuration.create
    }


    def "ProjectDescription"() {
        when:
        configuration.projectDescription('a')

        then:
        configuration.projectDescription == 'a'
    }

    def "ProjectHomePage"() {
        when:
        configuration.projectHomePage('a')

        then:
        configuration.projectHomePage == 'a'
    }

    def "PublicProject"() {
        when:
        configuration.publicProject(true)

        then:
        configuration.publicProject

        when:
        configuration.publicProject(false)

        then:
        !configuration.publicProject
    }

    def "ConfirmDelete"() {

        when:
        configuration.confirmDelete('a')

        then:
        configuration.confirmProjectDelete == 'a'
    }

    def "Remote names"() {
        when:
        configuration.remoteProjectUri("https://gitlab.com/dsowerby/wiggly")

        then:
        configuration.remoteProjectUri == "https://gitlab.com/dsowerby/wiggly"
    }

    def "RepoDeleteApprover"() {
        given:
        RemoteRepoDeleteApprover mockApprover = Mock(RemoteRepoDeleteApprover)

        when:
        true

        then: "default"
        configuration.repoDeleteApprover instanceof DefaultRemoteRepoDeleteApprover

        when:
        configuration.repoDeleteApprover(mockApprover)

        then:

        configuration.repoDeleteApprover == mockApprover
    }

    def "SetupFromOrigin"() {

        when:
        configuration.setupFromOrigin('https://github.com/davidsowerby/krail.git')

        then:
        configuration.remoteProjectUri == 'https://github.com/davidsowerby/krail.git'
    }

    def "setters"() {
        when: // default
        true

        then:
        configuration.active

        when:
        configuration.active(false)

        then:
        !configuration.active
    }


    def "exception if remote uri not specified"() {
        given:
        GitLocal local = Mock(GitLocal)

        when:
        configuration.validate(local)


        then:
        thrown GitPlusConfigurationException

    }


}
