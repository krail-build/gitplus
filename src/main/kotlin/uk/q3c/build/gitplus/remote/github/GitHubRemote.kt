package uk.q3c.build.gitplus.remote.github

import com.google.common.collect.ImmutableList
import com.google.inject.Inject
import com.jcabi.github.Branch
import com.jcabi.github.Coordinates
import com.jcabi.github.Github
import com.jcabi.github.Issue
import com.jcabi.github.Label
import com.jcabi.github.Repo
import com.jcabi.github.Repos
import com.jcabi.github.Search
import com.jcabi.http.Request
import org.eclipse.jgit.transport.CredentialsProvider
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider
import org.gitlab4j.api.models.Group
import org.gitlab4j.api.models.Project
import org.slf4j.LoggerFactory
import uk.q3c.build.gitplus.GitSHA
import uk.q3c.build.gitplus.gitplus.GitPlus
import uk.q3c.build.gitplus.gitplus.GitPlusConfigurationException
import uk.q3c.build.gitplus.local.GitBranch
import uk.q3c.build.gitplus.remote.AbstractGitRemote
import uk.q3c.build.gitplus.remote.GPIssue
import uk.q3c.build.gitplus.remote.GitRemote
import uk.q3c.build.gitplus.remote.GitRemoteConfiguration
import uk.q3c.build.gitplus.remote.GitRemoteException
import uk.q3c.build.gitplus.remote.GitRemoteUrlMapper
import uk.q3c.build.gitplus.remote.RemoteRequest
import uk.q3c.build.gitplus.remote.RemoteStatus
import uk.q3c.build.gitplus.remote.ServiceProvider
import java.io.IOException
import java.util.*

/**
 * Created by David Sowerby on 25 Oct 2016
 */
interface GitHubRemote : GitRemote


/**
 * Created by David Sowerby on 12 Feb 2016
 */
class DefaultGitHubRemote @Inject constructor(
        override val configuration: GitRemoteConfiguration,
        val gitHubProvider: GitHubProvider,
        val remoteRequest: RemoteRequest,
        override val urlMapper: GitHubUrlMapper) :
        AbstractGitRemote(),
        GitHubRemote,
        GitRemoteConfiguration by configuration,
        GitRemoteUrlMapper by urlMapper {

    override var projectCreated: Boolean = false
    override fun deleteGroup(group: Group, confirmationMessage: String) {
        throw UnsupportedOperationException("Groups not supported by GitHub")
    }


    override fun waitForRemoteRepo(newProject: Project, retries: Int, waitInterval: Long) {

    }


    private val log = LoggerFactory.getLogger(this.javaClass.name)
    private var currentTokenScope: GitRemote.TokenScope = GitRemote.TokenScope.CREATE_ISSUE
    override lateinit var parent: GitPlus

    init {
        urlMapper.remoteConfiguration = this
    }

    override fun setDefaultBranch() {
        log.warn("Default branch cannot be set")
    }

    override fun createGroup(): Group {
        throw UnsupportedOperationException("GitHub does not support groups")
    }


    override fun hasBranch(branch: GitBranch): Boolean {
        try {
            getBranch(branch)
            return true
        } catch (e: Exception) {
            return false
        }
    }


    override fun getIssue(issueNumber: Int): GPIssue {
        return getIssue(namespace, projectName, issueNumber)
    }

    override fun getIssue(remoteNamespace: String, remoteProjectName: String, issueNumber: Int): GPIssue {
        log.debug("Retrieving issue {} from {}", issueNumber, remoteProjectName)
        try {
            val repo = getGitHub(GitRemote.TokenScope.CREATE_ISSUE).repos().get(Coordinates.Simple(remoteNamespace, remoteProjectName))
            return GPIssue(repo.issues().get(issueNumber))
        } catch (e: Exception) {
            throw GitRemoteException("Unable to retrieve issue $issueNumber from $remoteProjectName", e)
        } catch (ae: AssertionError) {
            throw GitRemoteException("Unable to retrieve issue $issueNumber from $remoteProjectName, number or repo name is probably invalid")
        }

    }

    override val credentialsProvider: CredentialsProvider
        get() {
            try {
                val token = parent.apiTokenIssueCreate(ServiceProvider.GITHUB)
                return UsernamePasswordCredentialsProvider(token, "")
            } catch (e: Exception) {
                throw GitRemoteException("An api token is required in order to enable credentials", e)
            }

        }

    override fun apiStatus(): RemoteStatus {

        try {
            val token = parent.apiTokenIssueCreate(ServiceProvider.GITHUB)

            val response = remoteRequest.request(Request.GET, GitHubUrlMapper.STATUS_API_URL, token)
            val status = response.readObject().getString("status")
            when (status) {
                "good" -> return RemoteStatus.GREEN
                "minor" -> return RemoteStatus.YELLOW
                "major" -> return RemoteStatus.RED
                else -> return RemoteStatus.RED
            }
        } catch (e: Exception) {
            log.error("Unable to retrieve status from online service", e)
            return RemoteStatus.RED
        }

    }


    override fun createIssue(issueTitle: String, body: String, vararg labels: String): GPIssue {
        val issue = getRepo(GitRemote.TokenScope.CREATE_ISSUE).issues().create(issueTitle, body)

        val jsIssue = Issue.Smart(issue)
        jsIssue.labels().add(ImmutableList.copyOf(labels))
        return GPIssue(jsIssue)
    }

    private fun getGitHub(tokenScope: GitRemote.TokenScope): Github {
        return gitHubProvider.get(parent, tokenScope)
    }


    fun getRepo(tokenScope: GitRemote.TokenScope): Repo {
        val repos = getGitHub(tokenScope).repos()
        return repos.get(Coordinates.Simple(namespace, projectName))
    }

    /**
     * Creates the remote repo from the information in [configuration].  Note that there is no setting
     * for any option to control creation of a wiki - it is always created, even if [configuration] has useWiki set to false.  That said, you still
     * wll not be able to access the wiki via the API until you manually create the first page.
     */
    override fun createProject() {
        if (!projectCreated) {
            try {
                val settings = Repos.RepoCreate(projectName, !configuration.publicProject)
                settings.withDescription(configuration.projectDescription).withHomepage(configuration.projectHomePage)
                getGitHub(GitRemote.TokenScope.CREATE_REPO).repos().create(settings)
                if (configuration.mergeIssueLabels) {
                    mergeLabels()
                }
                projectCreated = true
            } catch (e: Exception) {
                throw GitRemoteException("Unable to create Repo", e)
            }
        } else {
            log.info("repeated call to create project ignored")
        }
    }


    override fun deleteProject() {
        try {
            if (configuration.deleteRepoApproved()) {
                getGitHub(GitRemote.TokenScope.DELETE_REPO).repos().remove(Coordinates.Simple(namespace, projectName))
            } else {
                throw GitRemoteException("Repo deletion not confirmed")
            }
        } catch (e: Exception) {
            throw GitRemoteException("Unable to delete remote repo", e)
        }

    }


    override fun listRepositoryNames(): Set<String> {
        val qualifier = "user:" + getRepo(GitRemote.TokenScope.CREATE_ISSUE).coordinates().user().removePrefix("//github.com/")
        val repoNames = TreeSet<String>()
        val repos = getGitHub(GitRemote.TokenScope.CREATE_ISSUE).search().repos(qualifier, "coordinates", Search.Order.ASC)
        repos.iterator().forEach { r -> repoNames.add(r.coordinates().repo()) }
        return repoNames
    }


    override fun mergeLabels() {
        mergeLabels(configuration.issueLabels)
    }


    override fun mergeLabels(labelsToMerge: Map<String, String>): Map<String, String> {
        val labels = getRepo(GitRemote.TokenScope.CREATE_ISSUE).labels()
        val iterate = labels.iterate()
        val iterator = iterate.iterator()

        //copy of map, we need to remove entries to keep track, and we may have been passed an ImmutableMap
        val lblsToMerge = TreeMap(labelsToMerge)

        //take existing labels first, update any colours which have changed, and remove labels that are not in the 'labelsToMerge'
        val existingToRemove = ArrayList<Label>()
        while (iterator.hasNext()) {
            val currentLabel = iterator.next()
            val currentName = currentLabel.name()
            //if this repo label is in new set as well, update the colour if, but only if not already the same (save unnecessary API calls)
            if (labelsToMerge.containsKey(currentName)) {
                val mergeColour = labelsToMerge[currentName]
                val smLabel = Label.Smart(currentLabel)
                if (smLabel.color() != mergeColour) {
                    smLabel.color(mergeColour)// this does the update (patch)
                }
                //processed, remove from merge set
                lblsToMerge.remove(currentName)
            } else {
                // it is not in the new set, note it for removal
                existingToRemove.add(currentLabel)
            }
        }

        //remove the unwanted
        for (label in existingToRemove) {
            log.debug("deleting label '{}", label.name())
            labels.delete(label.name())
        }

        //existing set is updated except for any new ones that need to be added
        //lblsToMerge now contains only those which have not been processed
        for ((key, value) in lblsToMerge) {
            try {
                labels.create(key, value)
            } catch (ioe: IOException) {
                log.warn("Failed to add new label '{}'", key, ioe)
            }

        }

        // do final check to make sure the two sets are the same, if different throw exception
        val checkMap = labelsAsMap

        if (checkMap != labelsToMerge) {
            throw GitRemoteException("Labels did not merge correctly")
        }
        return checkMap
    }


    override val labelsAsMap: Map<String, String>
        get() {
            val labels = getRepo(GitRemote.TokenScope.CREATE_ISSUE).labels()
            val map = TreeMap<String, String>()
            for (label in labels.iterate()) {
                val smLabel = Label.Smart(label)
                map.put(smLabel.name(), smLabel.color())
            }
            return map
        }

    override fun headCommit(branch: GitBranch): GitSHA {
        val developBranch = getBranch(branch)
        return GitSHA(developBranch.commit().sha())
    }

    override fun developHeadCommit(): GitSHA {
        return headCommit(GitBranch("develop"))
    }

    override fun prepare(parent: GitPlus) {
        this.parent = parent
        if (active) {
            validate(parent.local)
        }
    }

    override fun verifyFromLocal() {
        if (parent.local.active) {
            setupFromOrigin(parent.local.getOrigin())
        } else {
            throw GitPlusConfigurationException("GitRemote cannot be verified from a GitLocal which is not active")
        }
    }


    private fun getBranch(branch: GitBranch): Branch {
        val branchIterator = getRepo(GitRemote.TokenScope.CREATE_ISSUE).branches().iterate().iterator()
        while (branchIterator.hasNext()) {
            val candidate = branchIterator.next()
            if (candidate.name() == branch.name) {
                return candidate
            }
        }
        throw IOException("${branch.name} not found")
    }


}




