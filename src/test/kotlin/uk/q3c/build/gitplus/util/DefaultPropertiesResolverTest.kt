package uk.q3c.build.gitplus.util

import io.kotlintest.shouldBe
import io.kotlintest.shouldThrow
import io.kotlintest.specs.BehaviorSpec
import uk.q3c.build.gitplus.remote.ServiceProvider.GITHUB
import uk.q3c.build.gitplus.remote.ServiceProvider.GITLAB
import java.io.File


/**
 * Created by David Sowerby on 18 Oct 2018
 */
class DefaultPropertiesResolverTest : BehaviorSpec() {

    override fun isInstancePerTest(): Boolean {
        return true
    }

    init {
        Given("a properties file with all values present") {
            val testFile: File = propertiesFile(Any(), "/gitplus.properties")
            val loader = FilePropertiesLoader().source(testFile)
            val resolver = DefaultPropertiesResolver()
            resolver.loaders = mutableListOf(loader)

            When("we read it with correct keys") {
                Then("we get values expected") {
                    resolver.taggerEmail() == "me@there"
                    resolver.taggerName() == "me"
                    resolver.apiTokenIssueCreate(GITHUB).shouldBe("github issue create")
                    resolver.apiTokenRepoDelete(GITHUB).shouldBe("github repo delete")
                    resolver.apiTokenRepoCreate(GITHUB).shouldBe("github repo create")

                    resolver.apiTokenIssueCreate(GITLAB).shouldBe("gitlab issue create")
                    resolver.apiTokenRepoDelete(GITLAB).shouldBe("gitlab repo delete")
                    resolver.apiTokenRepoCreate(GITLAB).shouldBe("gitlab repo create")

                }
            }
        }
        Given("a properties file with a value missing") {
            val testFile: File = propertiesFile(Any(), "/gitplus2.properties")
            val loader = FilePropertiesLoader().source(testFile)
            val resolver = DefaultPropertiesResolver()
            resolver.loaders = mutableListOf(loader)

            When("we read it with an incorrect key") {
                Then("it should throw an exception") {
                    shouldThrow<MissingPropertyException> { resolver.apiTokenRepoDelete(GITLAB) }
                }
            }
        }

//        Given("no properties file, but environment variable set VIA RUN CONFIGURATION") {
//            val testFile: File = propertiesFile(Any(), "/gitplus2.properties")
//            val loader = FilePropertiesLoader().source(testFile)
//            val resolver = DefaultPropertiesResolver()
//            resolver.loaders = mutableListOf(loader, EnvironmentVariablePropertiesLoader())
//
//            When("we read it") {
//                Then("it return value from env") {
//                     resolver.apiTokenRepoDelete(GITLAB).shouldBe("gitlab token from environment")
//                }
//            }
//        }
    }
}

fun propertiesFile(ref: Any, fileName: String): File {

    val url = ref.javaClass.getResource(fileName).file
    return File(url)

}