package uk.q3c.build.gitplus.remote

import org.gitlab4j.api.models.Group
import uk.q3c.build.gitplus.gitplus.GitPlusConfigurationException
import uk.q3c.build.gitplus.local.GitLocal
import uk.q3c.build.gitplus.remote.github.GitHubRemote
import java.net.URI

/**
 *
 * A common interface for configuring a remote repo instance. Note that different [ServiceProvider]s (GitHub, BitBucket etc) may use the configuration differently.
 * See the companion [GitRemote] implementations, such as [GitHubRemote] for specifics
 *
 * Created by David Sowerby on 16 Oct 2016
 */
interface GitRemoteConfiguration {
    /**
     * Version of configuration structure. Has to be var to allow loading from JSON
     */
    var version: Int

    /**
     * If true, the remote Git instance is considered active.  Switch to false if you only want to work with the local
     * instance
     */
    var active: Boolean
    var projectDescription: String
    var projectHomePage: String
    var publicProject: Boolean

    var issueLabels: MutableMap<String, String>
    var mergeIssueLabels: Boolean
    var confirmProjectDelete: String
    var create: Boolean
    var repoDeleteApprover: RemoteRepoDeleteApprover
    var groupDeleteApprover: RemoteGroupDeleteApprover

    var remoteProjectUri: String

    /**
     * Not used by [GitHubRemote]: https://gitlab.com/krail-build/gitplus/issues/177
     */
    var defaultBranch: String

    fun deleteRepoApproved(): Boolean

    /**
     * If a GitLocal instance already has its origin set, this call can be used to set up some of the remote configuration from it
     */
    fun setupFromOrigin(origin: String)

    fun mergeIssueLabels(mergeIssueLabels: Boolean): GitRemoteConfiguration
    fun issueLabels(issueLabels: MutableMap<String, String>): GitRemoteConfiguration
    fun projectDescription(projectDescription: String): GitRemoteConfiguration
    fun projectHomePage(projectHomePage: String): GitRemoteConfiguration
    fun publicProject(publicProject: Boolean): GitRemoteConfiguration
    fun confirmDelete(confirmRemoteDelete: String): GitRemoteConfiguration
    fun repoDeleteApprover(repoDeleteApprover: RemoteRepoDeleteApprover): GitRemoteConfiguration

    /**
     * If true, a remote repository is created from this configuration
     */
    fun create(value: Boolean = true): GitRemoteConfiguration

    /**
     * Copy configuration values from another instance.  Cannot use the copy method provided by a data class, because
     * that is a copy constructor, returning a new instance.  Using a new instance breaks delegation
     */

    fun copyFrom(other: GitRemoteConfiguration)

    fun active(value: Boolean): GitRemoteConfiguration

    /**
     * Validates configuration settings, using local configuration to replace missing values where possible and appropriate
     *
     * @throws GitPlusConfigurationException if invalid configuration found
     */
    fun validate(local: GitLocal)

    /**
     * Confirms that a 'delete group' operation is approved (that is, the [RemoteGroupDeleteApprover] successfully validates the [confirmationMessage]
     */
    fun deleteGroupApproved(group: Group, confirmationMessage: String): Boolean

    fun remoteProjectUri(uri: URI): GitRemoteConfiguration
    fun remoteProjectUri(uri: String): GitRemoteConfiguration
}


/**
 * BitBucket is not actually implemented yet
 */
enum class ServiceProvider {
    GITHUB, BITBUCKET, GITLAB, NOT_SPECIFIED
}