package uk.q3c.build.gitplus.gitplus

import com.google.inject.Guice
import io.kotlintest.extensions.TestListener
import io.kotlintest.matchers.types.shouldBeInstanceOf
import io.kotlintest.specs.BehaviorSpec
import uk.q3c.build.gitplus.GitPlusModule
import uk.q3c.build.gitplus.remote.github.DefaultGitHubRemote
import uk.q3c.build.gitplus.remote.gitlab.DefaultGitLabRemote

/**
 * Created by David Sowerby on 14 Sep 2018
 */
class DefaultGitPlusTest3 : BehaviorSpec(), TestListener {

    init {
        Given("a GitPlus instance") {
            val injector = Guice.createInjector(GitPlusModule())
            val gitPlus = injector.getInstance(GitPlus::class.java)
            gitPlus.propertiesFromGradle()

            When("configured for GitLab") {
                gitPlus.remote.remoteProjectUri = "https://gitlab.com/wigglyuser/wiggly"
                gitPlus.evaluate()

                Then("remote is GitLab instance") {
                    gitPlus.remote.shouldBeInstanceOf<DefaultGitLabRemote>()
                }
            }


            When("configured for GitHub") {
                gitPlus.remote.remoteProjectUri = "https://github.com/wigglyuser/wiggly"
                gitPlus.evaluate()

                Then("remote is GitHub instance") {
                    gitPlus.remote.shouldBeInstanceOf<DefaultGitHubRemote>()
                }
            }


        }
    }
}